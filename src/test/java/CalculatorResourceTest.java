import org.junit.Test;
import resources.CalculatorResource;

import static org.junit.Assert.assertEquals;

public class CalculatorResourceTest{

    @Test
    public void testCalculate(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.calculate(expression));

        expression = " 300 - 99 ";
        assertEquals(201, calculatorResource.calculate(expression));

        expression = "1+2+3";
        assertEquals(6, calculatorResource.calculate(expression));
    }

    @Test
    public void testSum(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "100+300";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "300+99+1";
        assertEquals(400, calculatorResource.sum(expression));

        expression = "1+2+3+4+5+6";
        assertEquals(21, calculatorResource.sum(expression));
    }

    @Test
    public void testSubtraction(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "999-100";
        assertEquals(899, calculatorResource.subtraction(expression));

        expression = "20-2";
        assertEquals(18, calculatorResource.subtraction(expression));

        expression = "12-3-6";
        assertEquals(3, calculatorResource.subtraction(expression));
    }

    @Test
    public void testMultiplication(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "3*3";
        assertEquals(9, calculatorResource.calculate(expression));

        expression = "9*2";
        assertEquals(18, calculatorResource.calculate(expression));

        expression = "3*3*3";
        assertEquals(27, calculatorResource.calculate(expression));
    }

    @Test
    public void testDivision(){
        CalculatorResource calculatorResource = new CalculatorResource();

        String expression = "9/3";
        assertEquals(3, calculatorResource.calculate(expression));

        expression = "20/2";
        assertEquals(10, calculatorResource.calculate(expression));

        expression = "81/3/3/3";
        assertEquals(3, calculatorResource.calculate(expression));
    }
}
