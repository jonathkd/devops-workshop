package resources;

import dao.GroupChatDAO;
import data.GroupChat;
import data.Message;
import data.User;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import java.util.ArrayList;

/**
 * GroupChat resource exposed at "/groupchat" path
 */
@Path("/groupchat")
public class GroupChatResource {

    /**
     * GET method to get one groupchat with specified groupChatId
     * @param groupChatId of the chat to GET
     * @return GroupChat
     */
    @GET
    @Path ("{groupChatId}")
    @Produces (MediaType.APPLICATION_JSON)
    public GroupChat getGroupChat(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO gcd = new GroupChatDAO();
        GroupChat gc = gcd.getGroupChat(groupChatId);
        gc.setMessageList(gcd.getGroupChatMessages(groupChatId));
        gc.setUserList(gcd.getGroupChatUsers(groupChatId));
        return gc;
    }


    @GET
    @Path ("user/{userId}")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<GroupChat> getGroupChatByUserId(@PathParam("userId") int userId){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.getGroupChatByUserId(userId);
    }

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    public GroupChat postGroupChat(GroupChat gc){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.addGroupChat(gc);
    }

    @GET
    @Path("{groupChatId}/message")
    @Produces (MediaType.APPLICATION_JSON)
    public ArrayList<Message> getGroupChatMessage(@PathParam("groupChatId") int groupChatId){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.getGroupChatMessages(groupChatId);
    }

    @POST
    @Path("{groupChatId}/message")
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces (MediaType.APPLICATION_JSON)
    public Message postMessage(@PathParam("groupChatId") int groupChatId, Message
            message){
        GroupChatDAO gcd = new GroupChatDAO();
        return gcd.addMessage(groupChatId, message);
    }
}
